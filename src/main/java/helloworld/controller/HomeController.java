package helloworld.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcome() {
        return "index";
    }

    @RequestMapping(value = "/redirect_page", method = RequestMethod.GET)
    public String redirect() {
        return "redirect:final_page";
    }

    @RequestMapping(value = "/final_page", method = RequestMethod.GET)
    public String finalPage(Model model) {
        model.addAttribute("msg","Hello from controller");
        return "final";
    }
}