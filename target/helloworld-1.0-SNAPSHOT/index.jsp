<!DOCTYPE HTML>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Sping MVC Redirection </title>
</head>

<body>
        <form id="redirectionForm" action="redirect_page" method="GET">
            <input id="redirectBtn" type="submit" value="Redirect" />
        </form>
</body>

</html>